package com.dimoll.forecast.internal

enum class UnitSystem {
    METRIC, IMPERIAL
}