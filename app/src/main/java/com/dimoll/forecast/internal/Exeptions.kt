package com.dimoll.forecast.internal

import java.io.IOException

class NoConnectivityExeption : IOException()

class LocationPermissionGrantedException : Exception()

class DateNotFoundExeption : Exception()