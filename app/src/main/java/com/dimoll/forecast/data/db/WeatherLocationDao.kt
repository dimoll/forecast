package com.dimoll.forecast.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.dimoll.forecast.data.db.entity.WeatherLocation
import com.dimoll.forecast.data.db.entity.weather_location_id

@Dao
interface WeatherLocationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsert(weatherLocation: WeatherLocation)

    @Query("select * from weather_location where id=$weather_location_id")
    fun getLocation(): LiveData<WeatherLocation>

    @Query("select * from weather_location where id=$weather_location_id")
    fun getLocationNonLive(): WeatherLocation?
}