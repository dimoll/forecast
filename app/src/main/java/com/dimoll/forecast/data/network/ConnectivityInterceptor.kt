package com.dimoll.forecast.data.network

import okhttp3.Interceptor

interface ConnectivityInterceptor : Interceptor