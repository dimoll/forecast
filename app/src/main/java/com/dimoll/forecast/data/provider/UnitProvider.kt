package com.dimoll.forecast.data.provider

import com.dimoll.forecast.internal.UnitSystem

interface UnitProvider {
    fun getUnitsystem(): UnitSystem
}