package com.dimoll.forecast.data.provider

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.dimoll.forecast.internal.UnitSystem

const val unit_system = "UNIT_SYSTEM"

class UnitProviderImpl(
    context: Context
) : PreferenceProvider(context), UnitProvider {


    override fun getUnitsystem(): UnitSystem {
        val selectedName = preferences.getString(unit_system, UnitSystem.METRIC.name)

        return UnitSystem.valueOf(selectedName!!)
    }
}