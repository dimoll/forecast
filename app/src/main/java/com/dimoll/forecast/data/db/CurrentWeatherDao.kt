package com.dimoll.forecast.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.dimoll.forecast.data.db.entity.CurrentWeatherEntry
import com.dimoll.forecast.data.db.entity.current_weather_id
import com.dimoll.forecast.data.db.unitlocilized.current.ImperialCurrentWeatherEntry
import com.dimoll.forecast.data.db.unitlocilized.current.MetricCurrentWeatherEntry

@Dao
interface CurrentWeatherDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsert(weatherEntry: CurrentWeatherEntry)

    @Query("select * from current_weather where id = $current_weather_id")
    fun getWeatherMetric(): LiveData<MetricCurrentWeatherEntry>

    @Query("select * from current_weather where id = $current_weather_id")
    fun getWeatherImperial(): LiveData<ImperialCurrentWeatherEntry>
}