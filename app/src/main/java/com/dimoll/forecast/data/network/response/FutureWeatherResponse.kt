package com.dimoll.forecast.data.network.response


import com.dimoll.forecast.data.db.entity.WeatherLocation
import com.google.gson.annotations.SerializedName

data class FutureWeatherResponse(
    @SerializedName("forecast")
    val futureDaysEntries: ForecastDaysContainer,
    val location: WeatherLocation
)