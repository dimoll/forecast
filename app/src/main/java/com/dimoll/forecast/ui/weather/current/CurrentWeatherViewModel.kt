package com.dimoll.forecast.ui.weather.current

import androidx.lifecycle.ViewModel;
import com.dimoll.forecast.data.provider.UnitProvider
import com.dimoll.forecast.data.repository.ForecastRepository
import com.dimoll.forecast.internal.UnitSystem
import com.dimoll.forecast.internal.lazyDeferred
import com.dimoll.forecast.ui.base.WeatherViewModel

class CurrentWeatherViewModel(
    private val forecastRepository: ForecastRepository,
    unitProvider: UnitProvider
) : WeatherViewModel(forecastRepository, unitProvider) {

    val ldWeather by lazyDeferred {
        forecastRepository.getCurrentWeather(super.isMetricUnit)
    }
}
