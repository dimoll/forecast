package com.dimoll.forecast.ui.weather.future.list

import com.dimoll.forecast.data.provider.UnitProvider
import com.dimoll.forecast.data.repository.ForecastRepository
import com.dimoll.forecast.internal.lazyDeferred
import com.dimoll.forecast.ui.base.WeatherViewModel
import org.threeten.bp.LocalDate

class FutureListWeatherViewModel(
    private val forecastRepository: ForecastRepository,
    unitProvider: UnitProvider
) : WeatherViewModel(forecastRepository, unitProvider) {

    val weatherEntries by lazyDeferred {
        forecastRepository.getFutureWeatherList(LocalDate.now(), super.isMetricUnit)
    }
}

