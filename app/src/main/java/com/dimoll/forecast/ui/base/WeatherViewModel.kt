package com.dimoll.forecast.ui.base

import androidx.lifecycle.ViewModel
import com.dimoll.forecast.data.provider.UnitProvider
import com.dimoll.forecast.data.repository.ForecastRepository
import com.dimoll.forecast.internal.UnitSystem
import com.dimoll.forecast.internal.lazyDeferred

open class WeatherViewModel(
    private val forecastRepository: ForecastRepository,
    unitProvider: UnitProvider
) : ViewModel() {

    private val unitSystem = unitProvider.getUnitsystem()

    val isMetricUnit: Boolean
        get() = unitSystem == UnitSystem.METRIC


    val ldWeatherLocation by lazyDeferred {
        forecastRepository.getWeatherLocation()
    }
}