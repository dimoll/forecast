package com.dimoll.forecast.ui.weather.future.detail

import androidx.lifecycle.ViewModel;
import com.dimoll.forecast.data.provider.UnitProvider
import com.dimoll.forecast.data.repository.ForecastRepository
import com.dimoll.forecast.internal.lazyDeferred
import com.dimoll.forecast.ui.base.WeatherViewModel
import org.threeten.bp.LocalDate

class FutureDetailWeatherViewModel(
    private val detailDate: LocalDate,
    private val forecastRepository: ForecastRepository,
    unitProvider: UnitProvider
) : WeatherViewModel(forecastRepository, unitProvider) {

    val weather by lazyDeferred {
        forecastRepository.getFutureWeatherByDate(detailDate, super.isMetricUnit)
    }
}
